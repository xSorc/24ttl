$(function(){

startStopAudio();

var timer = 0;
var score = 0;

questJSON = "{\"questions\":[\n{\"questionNum\":\"1\",\"questionText\":\"\u041e\u043a, \u043d\u0430\u0447\u043d\u0435\u043c \u0441&nbsp;\u0441\u043e\u0432\u0441\u0435\u043c \u043f\u0440\u043e\u0441\u0442\u043e\u0433\u043e, \u0432&nbsp;\u043a\u0430\u0447\u0435\u0441\u0442\u0432\u0435 \u0440\u0430\u0437\u043c\u0438\u043d\u043a\u0438. \u041f\u0440\u043e\u0434\u043e\u043b\u0436\u0438\u0442\u0435 \u044d\u0442\u0443 \u043f\u0435\u0441\u043d\u044e \u0438\u0437&nbsp;\u0437\u043d\u0430\u043c\u0435\u043d\u0438\u0442\u043e\u0433\u043e \u0440\u0435\u043a\u043b\u0430\u043c\u043d\u043e\u0433\u043e \u0440\u043e\u043b\u0438\u043a\u0430:\", \"rightAnswer\": \"2\", \"answersArr\":[{\"answer\": \"\u0438 \u0433\u043e\u0432\u043e\u0440\u044e \u0423\u0425\u0422\u042b\"}, {\"answer\": \"\u0438 \u0433\u043e\u0432\u043e\u0440\u044e \u041a\u0420\u0410\u041d\u0422\u042b\"}, {\"answer\": \"\u0438 \u0433\u043e\u0432\u043e\u0440\u044e \u041c\u0415\u0420\u0421\u0418\"}], \"soundCode\":\"01\"},\n{\"questionNum\":\"2\",\"questionText\":\"\u0414\u0430\u043b\u0435\u0435&nbsp;&mdash; \u0445\u0438\u0442. \u0421\u043a\u0430\u0436\u0438\u0442\u0435, \u043a\u0442\u043e \u0438\u0441\u043f\u043e\u043b\u043d\u0438\u043b \u044d\u0442\u043e \u043f\u043e\u0442\u0440\u044f\u0441\u0430\u044e\u0449\u0435\u0435 \u0442\u0432\u043e\u0440\u0435\u043d\u0438\u0435 \u0438&nbsp;\u0434\u043b\u044f \u043a\u0430\u043a\u043e\u0433\u043e \u0431\u0440\u0435\u043d\u0434\u0430?\", \"rightAnswer\": \"0\", \"answersArr\":[{\"answer\": \"\u0422\u0438\u043c\u0430\u0442\u0438 \u0434\u043b\u044f \u0422\u0430\u043d\u0442\u0443\u043c \u0412\u0435\u0440\u0434\u0435\"}, {\"answer\": \"L\u2019One \u0434\u043b\u044f \u0411\u0440\u043e\u043c\u0433\u0435\u043a\u0441\u0438\u043d\u0430\"}, {\"answer\": \"\u041a\u0445\u0435\u043a\u0445\u0435, \u0432\u043e\u043e\u0431\u0449\u0435-\u0442\u043e \u044d\u0442\u043e \u0415\u0433\u043e\u0440 \u041a\u0440\u0438\u0434 \u0438 \u0435\u0433\u043e \u043b\u044e\u0431\u0438\u043c\u043e\u0435 \u0441\u0440\u0435\u0434\u0441\u0442\u0432\u043e \u0424\u0430\u0440\u0438\u043d\u0433\u043e\u0441\u0435\u043f\u0442\"}], \"soundCode\":\"02\"},\n{\"questionNum\":\"3\",\"questionText\":\"\u0424\u0430\u0440\u043c\u0430\u0446\u0435\u0432\u0442\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u0431\u0440\u0435\u043d\u0434\u044b \u0432\u043e\u043e\u0431\u0449\u0435 \u0431\u043e\u0433\u0430\u0442\u044b \u043d\u0430&nbsp;\u043c\u0443\u0437\u044b\u043a\u0430\u043b\u044c\u043d\u044b\u0435 \u0448\u0435\u0434\u0435\u0432\u0440\u044b. \u0414\u043b\u044f \u0447\u0435\u0433\u043e \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043b\u0438\u0441\u044c \u044d\u0442\u0438 \u0447\u0443\u0434\u0435\u0441\u043d\u044b\u0435 \u043d\u0435\u043c\u0435\u0446\u043a\u0438\u0435 \u043d\u0430\u043f\u0435\u0432\u044b?\", \"rightAnswer\": \"2\", \"answersArr\":[{\"answer\": \"\u0414\u043b\u044f \u0419\u043e\u043a\u0441\u0430\"}, {\"answer\": \"\u0419\u0430-\u0439\u0430, \u044d\u0442\u043e \u041b\u0430\u0437\u043e\u043b\u0432\u0430\u043d, \u044f \u0432\u043e\u043b\u044c\"}, {\"answer\": \"\u041d\u0435\u0442, \u043c\u0443\u0436\u0447\u0438\u043d\u0430 \u043f\u043e\u0435\u0442 \u043f\u0440\u043e \u0410\u043c\u0431\u0440\u043e\u0431\u0435\u043d\u0435\"}], \"soundCode\":\"03\"},\n{\"questionNum\":\"4\",\"questionText\":\"\u042d\u0442\u0430 \u043f\u0435\u0441\u043d\u044f \u0441\u0440\u0430\u0437\u0438\u043b\u0430 \u043d\u0430\u043f\u043e\u0432\u0430\u043b \u0430\u0443\u0434\u0438\u0442\u043e\u0440\u0438\u044e \u0442\u0435\u043b\u0435\u044d\u043a\u0440\u0430\u043d\u043e\u0432. \u041a\u0430\u043a\u043e\u043c\u0443&nbsp;\u0436\u0435 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0443 \u043e\u043d\u0430 \u043f\u043e\u0441\u0432\u044f\u0449\u0435\u043d\u0430?\", \"rightAnswer\": \"1\", \"answersArr\":[{\"answer\": \"\u041a\u043e\u0441\u043c\u0435\u0442\u0438\u043a\u0430 Maybelline\"}, {\"answer\": \"\u0413\u0438\u0433\u0438\u0435\u043d\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u0441\u0440\u0435\u0434\u0441\u0442\u0432\u0430 Always\"}, {\"answer\": \"\u0418\u043b\u0438 \u0432\u0441\u0435-\u0442\u0430\u043a\u0438 Kotex\"}], \"soundCode\":\"04\"},\n{\"questionNum\":\"5\",\"questionText\":\"\u0422\u0430\u043a, \u0442\u0435\u043f\u0435\u0440\u044c \u043d\u0435\u043c\u043d\u043e\u0433\u043e \u0441\u043b\u043e\u0436\u043d\u0435\u0435. \u0412\u0441\u043f\u043e\u043c\u043d\u0438\u0442\u0435, \u043a\u0442\u043e \u0438\u0437&nbsp;\u0444\u0430\u0441\u0442\u0444\u0443\u0434\u0430 \u0443\u0433\u043e\u0440\u0435\u043b \u043f\u043e&nbsp;\u0430\u0437\u0438\u0430\u0442\u0447\u0438\u043d\u0435? \u0418&nbsp;\u043d\u0435&nbsp;\u0441\u043c\u0435\u0439\u0442\u0435 \u0433\u0443\u0433\u043b\u0438\u0442\u044c!\", \"rightAnswer\": \"0\", \"answersArr\":[{\"answer\": \"McDonalds\"}, {\"answer\": \"KFC\"}, {\"answer\": \"Burger King\"}], \"soundCode\":\"05\"},\n{\"questionNum\":\"6\",\"questionText\":\"\u041b\u0430\u0434\u043d\u043e, \u0438\u043d\u043e\u0433\u0434\u0430 \u0438&nbsp;\u043d\u0435&nbsp;\u043e\u0447\u0435\u043d\u044c \u0442\u0440\u044d\u0448\u043e\u0432\u0430\u044f \u043c\u0443\u0437\u044b\u043a\u0430 \u0441\u0442\u0430\u043d\u043e\u0432\u0438\u0442\u0441\u044f \u043e\u0441\u043d\u043e\u0432\u043e\u0439 \u0434\u043b\u044f \u0440\u043e\u043b\u0438\u043a\u0430. \u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440, \u0421\u0435\u043c\u0435\u043d \u0421\u043b\u0435\u043f\u0430\u043a\u043e\u0432 \u043f\u0440\u0438\u0434\u0443\u043c\u0430\u043b \u0432\u043e\u0442 \u044d\u0442\u0443 \u0434\u043e\u0432\u043e\u043b\u044c\u043d\u043e \u043c\u0438\u043b\u0443\u044e \u043f\u0435\u0441\u043d\u044e \u0434\u043b\u044f \u043a\u043e\u0448\u0430\u0447\u044c\u0435\u0433\u043e \u043a\u043e\u0440\u043c\u0430. \u0417\u043d\u0430\u0435\u0442\u0435, \u0434\u043b\u044f \u043a\u0430\u043a\u043e\u0433\u043e?\", \"rightAnswer\": \"2\", \"answersArr\":[{\"answer\": \"Kitekat\"}, {\"answer\": \"Friskies\"}, {\"answer\": \"Whiskas\"}], \"soundCode\":\"06\"},\n{\"questionNum\":\"7\",\"questionText\":\"\u0412\u043e\u043e\u0431\u0449\u0435 \u0438\u0437\u0432\u0435\u0441\u0442\u043d\u044b\u0435 \u043c\u0443\u0437\u044b\u043a\u0430\u043d\u0442\u044b \u0434\u043e\u0432\u043e\u043b\u044c\u043d\u043e \u0447\u0430\u0441\u0442\u043e \u0437\u0430\u043f\u0438\u0441\u044b\u0432\u0430\u044e\u0442 \u0441\u0430\u0443\u043d\u0434\u0442\u0440\u0435\u043a \u043a&nbsp;\u0440\u0435\u043a\u043b\u0430\u043c\u0435. \u041a\u0442\u043e&nbsp;\u0436\u0435 \u0441\u0442\u0430\u043b \u0438\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u0435\u043c \u044d\u0442\u043e\u0433\u043e \u0445\u0438\u0442\u0430?\", \"rightAnswer\": \"2\", \"answersArr\":[{\"answer\": \"\u0414\u043c\u0438\u0442\u0440\u0438\u0439 \u041c\u0430\u043b\u0438\u043a\u043e\u0432\"}, {\"answer\": \"\u042e\u0440\u0438\u0439 \u041d\u0438\u043a\u043e\u043b\u0430\u0435\u0432\"}, {\"answer\": \"\u0412\u0430\u043b\u0435\u0440\u0438\u0439 \u041a\u0438\u043f\u0435\u043b\u043e\u0432\"}], \"soundCode\":\"07\"},\n{\"questionNum\":\"8\",\"questionText\":\"\u041d\u0443&nbsp;\u0438&nbsp;\u0435\u0449\u0435 \u043d\u0435\u043c\u043d\u043e\u0433\u043e \u043e&nbsp;\u0437\u0432\u0435\u0437\u0434\u0430\u0445: \u0441\u043a\u043e\u043b\u044c\u043a\u043e \u043b\u0435\u0442 \u0431\u044b\u043b\u043e \u041a\u043e\u043b\u0435 \u0411\u0430\u0441\u043a\u043e\u0432\u0443, \u043a\u043e\u0433\u0434\u0430 \u043e\u043d&nbsp;\u0441\u043f\u0435\u043b \u043d\u0435\u0442\u043b\u0435\u043d\u043a\u0443 \u043f\u0440\u043e \u0447\u0430\u0439 &laquo;\u0417\u043e\u043b\u043e\u0442\u0430\u044f \u0427\u0430\u0448\u0430&raquo;?\", \"rightAnswer\": \"1\", \"answersArr\":[{\"answer\": \"35, \u044d\u0442\u043e \u0435\u0433\u043e \u043b\u0443\u0447\u0448\u0438\u0439 \u0432\u043e\u0437\u0440\u0430\u0441\u0442!\"}, {\"answer\": \"30, \u044f \u0442\u043e\u0447\u043d\u043e \u043f\u043e\u043c\u043d\u044e, \u043e\u043d \u0442\u043e\u0433\u0434\u0430 \u043f\u0440\u0430\u0437\u0434\u043d\u043e\u0432\u0430\u043b \u044e\u0431\u0438\u043b\u0435\u0439\"}, {\"answer\": \"33, \u044d\u0442\u043e \u0432\u043e\u0437\u0440\u0430\u0441\u0442 \u0425\u0440\u0438\u0441\u0442\u0430, \u0434\u0430 \u0438 \u0441\u0430\u043c \u0411\u0430\u0441\u043a\u043e\u0432 \u2014 \u0411\u041e\u0413\"}], \"soundCode\":\"08\"},\n{\"questionNum\":\"9\",\"questionText\":\"\u0418\u043d\u043e\u0433\u0434\u0430 \u0440\u0435\u0436\u0438\u0441\u0441\u0435\u0440 \u0432\u044b\u0431\u0438\u0440\u0430\u0435\u0442 \u043d\u0435\u0432\u0435\u0440\u043e\u044f\u0442\u043d\u0443\u044e \u043c\u0443\u0437\u044b\u043a\u0443 \u0434\u043b\u044f \u0440\u043e\u043b\u0438\u043a\u0430. \u041e\u0441\u043e\u0431\u0435\u043d\u043d\u043e, \u0435\u0441\u043b\u0438 \u044d\u0442\u043e \u0442\u0430\u043a\u043e\u0439 \u0440\u0435\u0436\u0438\u0441\u0441\u0435\u0440 \u043a\u0430\u043a Spike Jonze. \u041e\u0441\u043d\u043e\u0432\u043e\u0439 \u0440\u0435\u043a\u043b\u0430\u043c\u044b \u043a\u0430\u043a\u043e\u0433\u043e \u043f\u0430\u0440\u0444\u044e\u043c\u0430 \u043e\u043d&nbsp;\u0440\u0435\u0448\u0438\u043b \u0441\u0434\u0435\u043b\u0430\u0442\u044c \u044d\u0442\u043e\u0442 \u0442\u0440\u0435\u043a?\", \"rightAnswer\": \"0\", \"answersArr\":[{\"answer\": \"\u042d\u0442\u043e \u043d\u043e\u0432\u044b\u0439 \u0430\u0440\u043e\u043c\u0430\u0442 Kenzo\"}, {\"answer\": \"\u0414\u0435\u0432\u0443\u0448\u043a\u0430 \u0441\u043a\u0430\u043a\u0430\u043b\u0430 \u043f\u043e\u0434 \u044d\u0442\u0443 \u043c\u0443\u0437\u044b\u043a\u0443 \u0432 \u0440\u043e\u043b\u0438\u043a\u0435 Etro\"}, {\"answer\": \"\u0422\u0430\u043a\u043e\u0435 \u043c\u043e\u0433\u0443\u0442 \u043f\u043e\u0437\u0432\u043e\u043b\u0438\u0442\u044c \u0441\u0435\u0431\u0435 \u0442\u043e\u043b\u044c\u043a\u043e  Gucci (\u0438\u0445 \u043c\u0430\u0433\u0430\u0437\u0438\u043d \u0432 \u0421\u0430\u043d\u043a\u0442-\u041f\u0435\u0442\u0435\u0440\u0431\u0443\u0440\u0433\u0435)\"}], \"soundCode\":\"09\"},\n{\"questionNum\":\"10\",\"questionText\":\"\u0418&nbsp;\u043d\u0430\u043a\u043e\u043d\u0435\u0446 \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0432\u043e\u043f\u0440\u043e\u0441, \u043d\u0430\u0448 \u043b\u044e\u0431\u0438\u043c\u044b\u0439: \u0412\u044b&nbsp;\u0436\u0435 \u0432&nbsp;\u043a\u0443\u0440\u0441\u0435, \u0434\u043b\u044f \u043a\u0430\u043a\u043e\u0433\u043e \u0431\u0440\u0435\u043d\u0434\u0430 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043b\u0438\u0441\u044c \u044d\u0442\u0438 \u0437\u043d\u0430\u043a\u043e\u043c\u044b\u0435 \u0441&nbsp;\u0434\u0435\u0442\u0441\u0442\u0432\u0430 \u0437\u0432\u0443\u043a\u0438?\", \"rightAnswer\": \"2\", \"answersArr\":[{\"answer\": \"\u0410\u0439\u0440\u043d \u0411\u0440\u044e \u0438 \u0441\u0442\u0440\u0430\u0443\u0441\u044b! \u041e, \u0434\u0430!\"}, {\"answer\": \"\u042d\u0442\u043e Pepsi \u0432 \u043f\u0435\u0440\u0438\u043e\u0434 \u0440\u0430\u0441\u0446\u0432\u0435\u0442\u0430!\"}, {\"answer\": \"Mentos \u0438 \u043e\u0432\u0446\u044b, \u043d\u0438\u043a\u043e\u0433\u0434\u0430 \u043d\u0435 \u0437\u0430\u0431\u0443\u0434\u0443\"}], \"soundCode\":\"10\"},\n{\"questionNum\":\"11\",\"questionText\":\"\u0418&nbsp;\u043d\u0430\u043a\u043e\u043d\u0435\u0446 \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0432\u043e\u043f\u0440\u043e\u0441, \u043d\u0430\u0448 \u043b\u044e\u0431\u0438\u043c\u044b\u0439: \u0412\u044b&nbsp;\u0436\u0435 \u0432&nbsp;\u043a\u0443\u0440\u0441\u0435, \u0434\u043b\u044f \u043a\u0430\u043a\u043e\u0433\u043e \u0431\u0440\u0435\u043d\u0434\u0430 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043b\u0438\u0441\u044c \u044d\u0442\u0438 \u0437\u043d\u0430\u043a\u043e\u043c\u044b\u0435 \u0441&nbsp;\u0434\u0435\u0442\u0441\u0442\u0432\u0430 \u0437\u0432\u0443\u043a\u0438?\", \"rightAnswer\": \"2\", \"answersArr\":[{\"answer\": \"\u0410\u0439\u0440\u043d \u0411\u0440\u044e \u0438 \u0441\u0442\u0440\u0430\u0443\u0441\u044b! \u041e, \u0434\u0430!\"}, {\"answer\": \"\u042d\u0442\u043e Pepsi \u0432 \u043f\u0435\u0440\u0438\u043e\u0434 \u0440\u0430\u0441\u0446\u0432\u0435\u0442\u0430!\"}, {\"answer\": \"Mentos \u0438 \u043e\u0432\u0446\u044b, \u043d\u0438\u043a\u043e\u0433\u0434\u0430 \u043d\u0435 \u0437\u0430\u0431\u0443\u0434\u0443\"}], \"soundCode\":\"10\"}\n]}";
resultJSON = "{\"results\": [\n{\"result\": \"0\", \"title\": \"0\/10\", \"text\": \"\u0412\u044b \u0442\u043e\u0447\u043d\u043e \u043a\u043e\u0433\u0434\u0430-\u043b\u0438\u0431\u043e \u0441\u043c\u043e\u0442\u0440\u0435\u043b\u0438 \u0440\u0435\u043a\u043b\u0430\u043c\u0443?  \u041d\u0435\u0442, \u0432\u044b \u0442\u043e\u0447\u043d\u043e \u0447\u0435\u043b\u043e\u0432\u0435\u043a? (\u0432\u0435\u0434\u044c \u043e\u0442 \u044d\u0442\u0438\u0445 \u043c\u0435\u043b\u043e\u0434\u0438\u0439 \u043d\u0435 \u043c\u043e\u0433\u043b\u043e \u0441\u043a\u0440\u044b\u0442\u044c\u0441\u044f  \u043d\u0438 \u043e\u0434\u043d\u043e \u0436\u0438\u0432\u043e\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u043e)\"},\n{\"result\": \"1\", \"title\": \"1\/10\", \"text\": \"\u0412\u044b \u0442\u043e\u0447\u043d\u043e \u043a\u043e\u0433\u0434\u0430-\u043b\u0438\u0431\u043e \u0441\u043c\u043e\u0442\u0440\u0435\u043b\u0438 \u0440\u0435\u043a\u043b\u0430\u043c\u0443?  \u041d\u0435\u0442, \u0432\u044b \u0442\u043e\u0447\u043d\u043e \u0447\u0435\u043b\u043e\u0432\u0435\u043a? (\u0432\u0435\u0434\u044c \u043e\u0442 \u044d\u0442\u0438\u0445 \u043c\u0435\u043b\u043e\u0434\u0438\u0439 \u043d\u0435 \u043c\u043e\u0433\u043b\u043e \u0441\u043a\u0440\u044b\u0442\u044c\u0441\u044f  \u043d\u0438 \u043e\u0434\u043d\u043e \u0436\u0438\u0432\u043e\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u043e)\"},\n{\"result\": \"2\", \"title\": \"2\/10\", \"text\": \"\u0412\u044b \u0442\u043e\u0447\u043d\u043e \u043a\u043e\u0433\u0434\u0430-\u043b\u0438\u0431\u043e \u0441\u043c\u043e\u0442\u0440\u0435\u043b\u0438 \u0440\u0435\u043a\u043b\u0430\u043c\u0443?  \u041d\u0435\u0442, \u0432\u044b \u0442\u043e\u0447\u043d\u043e \u0447\u0435\u043b\u043e\u0432\u0435\u043a? (\u0432\u0435\u0434\u044c \u043e\u0442 \u044d\u0442\u0438\u0445 \u043c\u0435\u043b\u043e\u0434\u0438\u0439 \u043d\u0435 \u043c\u043e\u0433\u043b\u043e \u0441\u043a\u0440\u044b\u0442\u044c\u0441\u044f  \u043d\u0438 \u043e\u0434\u043d\u043e \u0436\u0438\u0432\u043e\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u043e)\"},\n{\"result\": \"3\", \"title\": \"3\/10\", \"text\": \"\u0427\u0442\u043e \u0436, \u0435\u0449\u0435 \u043f\u0430\u0440\u0443 \u0442\u0440\u0435\u043d\u0438\u0440\u043e\u0432\u043e\u043a  \u043d\u0430 \u0441\u043f\u0435\u0446\u043a\u0443\u0440\u0441\u0430\u0445 \u0412\u0430\u043b\u0434\u0438\u0441\u0430 \u041f\u0435\u043b\u044c\u0448\u0430 (\u043a\u043e\u0442\u043e\u0440\u044b\u0445 \u043d\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0443\u0435\u0442), \u0438 \u0432\u044b \u0441\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u0441\u044c!\"},\n{\"result\": \"4\", \"title\": \"4\/10\", \"text\": \"\u0427\u0442\u043e \u0436, \u0435\u0449\u0435 \u043f\u0430\u0440\u0443 \u0442\u0440\u0435\u043d\u0438\u0440\u043e\u0432\u043e\u043a  \u043d\u0430 \u0441\u043f\u0435\u0446\u043a\u0443\u0440\u0441\u0430\u0445 \u0412\u0430\u043b\u0434\u0438\u0441\u0430 \u041f\u0435\u043b\u044c\u0448\u0430 (\u043a\u043e\u0442\u043e\u0440\u044b\u0445 \u043d\u0435 \u0441\u0443\u0449\u0435\u0441\u0442\u0432\u0443\u0435\u0442), \u0438 \u0432\u044b \u0441\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u0441\u044c!\"},\n{\"result\": \"5\", \"title\": \"5\/10\", \"text\": \"\u041c\u0430\u043c\u0431\u0443! \u0424\u0440\u0443\u043f\u0438\u0441! \u041b\u044e\u0431\u0438\u043c \u043c\u044b \u0432\u0441\u0435! \u0438 \u0432\u044b, \u043f\u043e\u0445\u043e\u0436\u0435, \u0442\u043e\u0436\u0435. \u041f\u043e\u0437\u0434\u0440\u0430\u0432\u043b\u044f\u0435\u043c \u0441 \u043d\u0435\u043f\u043b\u043e\u0445\u0438\u043c \u0440\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442\u043e\u043c.\"},\n{\"result\": \"6\", \"title\": \"6\/10\", \"text\": \"\u041c\u0430\u043c\u0431\u0443! \u0424\u0440\u0443\u043f\u0438\u0441! \u041b\u044e\u0431\u0438\u043c \u043c\u044b \u0432\u0441\u0435! \u0438 \u0432\u044b, \u043f\u043e\u0445\u043e\u0436\u0435, \u0442\u043e\u0436\u0435. \u041f\u043e\u0437\u0434\u0440\u0430\u0432\u043b\u044f\u0435\u043c \u0441 \u043d\u0435\u043f\u043b\u043e\u0445\u0438\u043c \u0440\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442\u043e\u043c.\"},\n{\"result\": \"7\", \"title\": \"7\/10\", \"text\": \"\u0412\u0430\u0448\u0438 \u0443\u0448\u0438 \u043f\u0440\u043e\u0448\u043b\u0438 \u0447\u0435\u0440\u0435\u0437 \u043c\u043d\u043e\u0433\u043e\u0435, \u0438 \u043e\u043d\u0438 \u043d\u0435 \u0441\u043f\u043e\u0441\u043e\u0431\u043d\u044b \u044d\u0442\u043e \u043e\u0431\u0440\u0430\u0442\u043d\u043e \u0440\u0430\u0437\u0443\u0441\u043b\u044b\u0448\u0430\u0442\u044c. \u0423\u0440\u0430, \u043d\u0430\u043a\u043e\u043d\u0435\u0446-\u0442\u043e \u044d\u0442\u043e\u0442 \u043e\u043f\u044b\u0442 \u0441\u043c\u043e\u0433 \u043f\u0440\u0438\u0433\u043e\u0434\u0438\u0442\u044c\u0441\u044f!\"},\n{\"result\": \"8\", \"title\": \"8\/10\", \"text\": \"\u0412\u0430\u0448\u0438 \u0443\u0448\u0438 \u043f\u0440\u043e\u0448\u043b\u0438 \u0447\u0435\u0440\u0435\u0437 \u043c\u043d\u043e\u0433\u043e\u0435, \u0438 \u043e\u043d\u0438 \u043d\u0435 \u0441\u043f\u043e\u0441\u043e\u0431\u043d\u044b \u044d\u0442\u043e \u043e\u0431\u0440\u0430\u0442\u043d\u043e \u0440\u0430\u0437\u0443\u0441\u043b\u044b\u0448\u0430\u0442\u044c. \u0423\u0440\u0430, \u043d\u0430\u043a\u043e\u043d\u0435\u0446-\u0442\u043e \u044d\u0442\u043e\u0442 \u043e\u043f\u044b\u0442 \u0441\u043c\u043e\u0433 \u043f\u0440\u0438\u0433\u043e\u0434\u0438\u0442\u044c\u0441\u044f!\"},\n{\"result\": \"9\", \"title\": \"9\/10\", \"text\": \"\u0421\u0442\u043e\u0439\u0442\u0435. \u0412\u043e\u0437\u043c\u043e\u0436\u043d\u043e, \u0432\u044b \u0438 \u0435\u0441\u0442\u044c \u0442\u043e\u0442 \u0447\u0435\u043b\u043e\u0432\u0435\u043a, \u043a\u043e\u0442\u043e\u0440\u044b\u0438\u0306 \u043f\u0440\u0438\u0434\u0443\u043c\u0430\u043b \u0432\u0441\u0435 \u044d\u0442\u0438 \u0434\u0443\u0448\u0435\u0440\u0430\u0437\u0434\u0438\u0440\u0430\u044e\u0449\u0438\u0435 \u0434\u0436\u0438\u043d\u0433\u043b\u044b? \u041f\u043e\u0434\u043e\u0437\u0440\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u044d\u043d\u0446\u0438\u043a\u043b\u043e\u043f\u0435\u0434\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u043f\u043e\u0437\u043d\u0430\u043d\u0438\u044f. \u0412\u044b \u0432 \u0447\u0438\u0441\u043b\u0435 \u043b\u0443\u0447\u0448\u0438\u0445!\"},\n{\"result\": \"10\", \"title\": \"10\/10\", \"text\": \"\u0421\u0442\u043e\u0439\u0442\u0435. \u0412\u043e\u0437\u043c\u043e\u0436\u043d\u043e, \u0432\u044b \u0438 \u0435\u0441\u0442\u044c \u0442\u043e\u0442 \u0447\u0435\u043b\u043e\u0432\u0435\u043a, \u043a\u043e\u0442\u043e\u0440\u044b\u0438\u0306 \u043f\u0440\u0438\u0434\u0443\u043c\u0430\u043b \u0432\u0441\u0435 \u044d\u0442\u0438 \u0434\u0443\u0448\u0435\u0440\u0430\u0437\u0434\u0438\u0440\u0430\u044e\u0449\u0438\u0435 \u0434\u0436\u0438\u043d\u0433\u043b\u044b? \u041f\u043e\u0434\u043e\u0437\u0440\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u044d\u043d\u0446\u0438\u043a\u043b\u043e\u043f\u0435\u0434\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u043f\u043e\u0437\u043d\u0430\u043d\u0438\u044f. \u0412\u044b \u0432 \u0447\u0438\u0441\u043b\u0435 \u043b\u0443\u0447\u0448\u0438\u0445!\"}\n]\n}";

var interval = setInterval(function(){
    if (timer==0) return;
    document.getElementById('clock').innerHTML = ((new Date().getTime()-timer)/1000).toFixed(1);
},100);
// $('#start').click(function(){
// $('#fb-login').click(function(){
// 	FB.login(function(response) {
// 		if (response.status === 'connected') {
// 			// $('.user-section').show();

// 			fields = ['id', 'first_name', 'last_name', 'link', 'gender', 'picture', 'email'];
// 			FB.api('/me?fields=' + fields.join(','), function(res) {
// 				user_id = res.id;
// 				$.ajax({
// 					url: "functions/add_user.php",
// 					type: 'POST',
// 					data: {"user_id" : res.id, "first_name" : res.first_name, "last_name" : res.last_name, "link": res.link, "picture": res.picture.data.url},
// 					success: function(data) {
// 						if (data == 'error') {
// 							$('.login-section').hide();
// 							$('.user-section').hide();
// 							$('.error-section').show();
// 							$('.error-section .text').html(res.first_name+', вы уже проходили этот тест. Оставьте другим шанс :)');
// 						} else {
// 							$('.login-section').hide();
// 							gameStart();
// 						}
// 					}
// 				});
// 			});
			
// 		}
// 	});

// })
$('#fb-login').click(function(){
	FB.getLoginStatus(function(response) {
            if (!response.authResponse) {
				FB.login(function(response) {
					if (response.status === 'connected') {
						fields = ['id', 'first_name', 'last_name', 'link', 'gender', 'picture', 'email'];
						FB.api('/me?fields=' + fields.join(','), function(res) {
							user_id = res.id;
							$.ajax({
								url: "functions/add_user.php",
								type: 'POST',
								data: {"user_id" : res.id, "first_name" : res.first_name, "last_name" : res.last_name, "link": res.link, "picture": res.picture.data.url},
								success: function(data) {
									if (data == 'error') {
										$('.login-section').hide();
										$('.user-section').hide();
										$('.error-section').show();
										dest = $('.error-section').offset().top;
										$('.error-section .text').html(res.first_name+', вы уже проходили этот тест. Оставьте другим шанс :)');
										$('html,body)').animate({scrollTop: dest}, 200);
									} else {
										$('.login-section').hide();
										gameStart();
									}
								}
							});
						});
					}
				});
			} else {
				if (response.status === 'connected') {
					fields = ['id', 'first_name', 'last_name', 'link', 'gender', 'picture', 'email'];
					FB.api('/me?fields=' + fields.join(','), function(res) {
						user_id = res.id;
						$.ajax({
							url: "functions/add_user.php",
							type: 'POST',
							data: {"user_id" : res.id, "first_name" : res.first_name, "last_name" : res.last_name, "link": res.link, "picture": res.picture.data.url},
							success: function(data) {
								if (data == 'error') {
									$('.login-section').hide();
									$('.user-section').hide();
									$('.error-section').show();
									$('.error-section .text').html(res.first_name+', вы уже проходили этот тест. Оставьте другим шанс :)');
								} else {
									$('.login-section').hide();
									gameStart();
								}
							}
						});
					});
				}
			}
	});
	return false;
})
$(window).resize(function(){
	// resize();
});

function timelineInit() {
	setTimeout(function(){
		$('.played').show();
	},500);
	audio = $('audio');
	duration = audio.prop('duration');
	width = $('.timeline').width();
	playedWidth = 0;
	timl = setInterval(function(){
		currentTime = audio.prop('currentTime');
		playedWidth = currentTime / duration * 100;
		$('.played').css('width', playedWidth+'%')
		// console.log(duration,currentTime);
	},500);

}
function fbLogInCheck() {
	$(window).load(function(){
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				$('.login-section').hide();
				$('.user-section').show();
				
				var fields = ['id', 'first_name', 'last_name', 'link', 'gender', 'picture', 'email'];
				FB.api('/me?fields=' + fields.join(','), function(res) {
					user_id = res.id;
					$('.status').html('Привет, ' +res.first_name +' '+res.last_name);
				});
			}
		});
	});
}
function gameStart() {
	$('.user-section').hide();
	$('.game-section').show();
	$('.popup .button').click(function(){
		$.ajax({
			url: "functions/start.php",
			type: 'POST',
			data: {"user_id" : user_id}
		});
		$('.popup').hide();
		$('.overlay').hide();
		document.getElementById('audio').play();
		timer = 0;
		timer = new Date().getTime();
		setTimeout(function() {
			timelineInit();
		},500);
		return false;
	});
	$('.game-section').on('click','.variant', function(){
		if (!($(this).hasClass('disabled'))) {
			clearInterval(interval);
			$('.variant').addClass('disabled');
			var question = $('.question').attr('data-questionid');
			var answer = $(this).attr('data-answerid');
			var self = $(this);
			var rightAnswer = JSON.parse(questJSON).questions[question].rightAnswer;
			if(rightAnswer == answer) {
				self.addClass('true');
				score = score + 1;
			} else {
				self.addClass('false');
			}
			// console.log("score: " +score);
			setTimeout(function(){
				getQuestion();
			},500);
		}

		return false;
	});
}
function getQuestion() {
	var question = $('.question').attr('data-questionid');
	var questionidNew = parseInt(question) + 1;
	// console.log(questionidNew);
	if (questionidNew > 10) {
		document.getElementById('audio').pause();
		var title = JSON.parse(resultJSON).results[score].title;
		var text = JSON.parse(resultJSON).results[score].text;
		var result = JSON.parse(resultJSON).results[score].result;
		$('.game-section').hide();
		$('.result-section').show();
		$('.result-section .title').html(title);
		$('.result-section .text').html(text);
		$('.result-section .clearfix img').attr('src','img/share/'+result+'.png');
		$('.share-block').html('<a class="fb-share" onclick="Share.facebook(\'https://10.24ttl.ru/?share='+result+'\')"><img src="img/fb.svg" alt="" /> Facebook</a><a onclick="Share.vkontakte(\'https://10.24ttl.ru/\',\'10 громких лет 24ttl\',\'https://10.24ttl.ru/img/share'+result+'.png\',\'Угадай музыку из рекламы и получи билет на интенсив и вечеринку\')" class="vk-share"><img src="img/vk.svg" alt="" />ВКонтакте</a>');
		
		var fields = ['id', 'first_name', 'last_name', 'link', 'gender', 'picture', 'email'];
		clearInterval(interval);
		clearInterval(timl);
		FB.api('/me?fields=' + fields.join(','), function(res) {
			$.ajax({
				url: "functions/finish.php",
				type: 'POST',
				data: {"user_id" : res.id, "score": score}
			});
		});
	} else {
		$('.played').hide();
		clearInterval(timl);
		clearInterval(interval);
		$('.question').attr('data-questionid',questionidNew);
		var questionText = JSON.parse(questJSON).questions[questionidNew].questionText;
		var soundCode = JSON.parse(questJSON).questions[questionidNew].soundCode;
		var answerArr = JSON.parse(questJSON).questions[questionidNew].answersArr;

		$('.question-number').html(questionidNew+'/10');
		$('.question-text').html(questionText);
		$('audio').attr('src','https://10.24ttl.ru/audio/'+soundCode+'.mp3');
		$('.answers').html('');
		for(i=0;i<answerArr.length;i++) {
			$('.answers').append('<a href="#" data-answerid="'+i+'" class="variant">'+answerArr[i].answer+'</a> ');
		}
		$('#play').hide();
		$('#pause').show();
		document.getElementById('audio').play();
		setTimeout(function() {
			timelineInit();
		},500);

		
		interval = setInterval(function(){
			if (timer==0) return;
			document.getElementById('clock').innerHTML = ((new Date().getTime()-timer)/1000).toFixed(1);
		},100);
		timer = 0;
		timer = new Date().getTime();
	}
}
function startStopAudio() {
	$('#pause').click(function(){
		document.getElementById('audio').pause();
		$('#pause').hide();
		$('#play').show();
		return false;
	});
	$('#play').click(function(){
		document.getElementById('audio').play();
		$('#play').hide();
		$('#pause').show();
		return false;
	});
}

function startTime() {
 
    var milisec=0;
	var seconds=0;
	
    $('#clock').text('0.0');
	function display(){;
	if (milisec>=9){;
		milisec=0;
		seconds+=1;
	} else {
		milisec+=1;
		$('#clock').text(seconds+"."+milisec);
		setTimeout("display()",100);
	};
	display();
  };
}
function fbLogIn() {
	$('#fb-login').click(function(){
		FB.login(function(response) {
			if (response.status === 'connected') {
				$('.login-section').hide();
				$('.user-section').show();
				
				var fields = ['id', 'first_name', 'last_name', 'link', 'gender', 'picture', 'email'];
				FB.api('/me?fields=' + fields.join(','), function(res) {
					user_id = res.id;
					$('.status').html('Привет, ' +res.first_name +' '+res.last_name);
				});
			}
			return false;
		});
	});
}
Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.popup(url);
	},
	facebook: function(purl) {
		url  = 'http://www.facebook.com/sharer.php?';
		url += 'u='       + encodeURIComponent(purl);
		Share.popup(url);
	},
	twitter: function(purl, ptitle) {
		url  = 'https://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url);
	},
	popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};
$('#fb-logout').click(function(){
	$('.login-section').show();
	$('.user-section').hide();
	FB.logout(function(response) {
		if (response.status !== 'connected') {
				$('.status').html('вы разлогинены');

			// $('.auth-section').show();
			// $('.crossword-section').addClass('no-active');
		}
	});
	return false;
});
});
